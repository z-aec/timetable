-- MySQL dump 10.13  Distrib 5.7.11, for Linux (x86_64)
--
-- Host: localhost    Database: timetable
-- ------------------------------------------------------
-- Server version	5.7.11-0ubuntu6

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `event`
--

DROP TABLE IF EXISTS `event`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event` (
  `event_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_id` int(11) NOT NULL,
  `event_name` text,
  `summary` text,
  `description` mediumtext,
  `start_date` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `type` varchar(255) NOT NULL,
  `is_finished` tinyint(1) NOT NULL DEFAULT '0',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `change_reason` text,
  `last_changed_by` int(11) NOT NULL,
  PRIMARY KEY (`event_id`),
  KEY `event_project_project_id_fk` (`project_id`),
  KEY `event_event_type_type_name_fk` (`type`),
  KEY `event_user_id_fk` (`last_changed_by`),
  CONSTRAINT `event_event_type_type_name_fk` FOREIGN KEY (`type`) REFERENCES `event_type` (`type_name`) ON UPDATE CASCADE,
  CONSTRAINT `event_project_project_id_fk` FOREIGN KEY (`project_id`) REFERENCES `project` (`project_id`) ON UPDATE CASCADE,
  CONSTRAINT `event_user_id_fk` FOREIGN KEY (`last_changed_by`) REFERENCES `user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event`
--

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;
INSERT INTO `event` VALUES (1,1,'asf','gasdfgafsg','asgargrwegrwegwergwergvwer','2018-08-25 12:48:23','Meeting',0,0,'',1),(2,1,'adsf','fasdfa','asdfasd','2018-09-02 00:00:00','Briefing',1,0,'',1),(3,1,'4681365','fasdfa','asdfasd','2018-09-02 00:00:00','Briefing',0,1,'',1),(4,4,'fasdfe','fds','asdf','2018-09-02 00:00:00','Briefing',1,0,'',1),(5,1,'this is a test','asdgf','aetvbvcb','2018-09-15 00:00:00','Conference',1,0,'',1),(6,1,'testing','asdfgasd','asdgfasd','2018-09-05 00:00:00','Meeting',0,0,'',1),(7,1,'<b>test xss</b>','<b>test xss</b>','<b>test xss</b>','2018-08-27 21:00:17','Briefing',0,0,NULL,1);
/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER insert_event AFTER INSERT ON event
FOR EACH ROW BEGIN
  INSERT INTO event_history (change_type, change_ts, change_by, event_id, project_id, event_name, summary, description, start_date, type, is_finished, is_deleted, change_reason)
  VALUES (0, NOW(), NEW.last_changed_by, NEW.event_id, NEW.project_id, NEW.event_name, NEW.summary, NEW.description, NEW.start_date, NEW.type, NEW.is_finished, NEW.is_deleted, NEW.change_reason);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER update_event AFTER UPDATE ON event
FOR EACH ROW BEGIN
  INSERT INTO event_history (change_type, change_ts, change_by, event_id, project_id, event_name, summary, description, start_date, type, is_finished, is_deleted, change_reason)
  VALUES (1, NOW(), NEW.last_changed_by, NEW.event_id, NEW.project_id, NEW.event_name, NEW.summary, NEW.description, NEW.start_date, NEW.type, NEW.is_finished, NEW.is_deleted, NEW.change_reason);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `event_history`
--

DROP TABLE IF EXISTS `event_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_history` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  `change_type` tinyint(4) NOT NULL DEFAULT '0',
  `change_ts` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `change_by` int(11) NOT NULL,
  `event_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `event_name` text,
  `summary` text,
  `description` text,
  `start_date` datetime DEFAULT NULL,
  `type` varchar(32) DEFAULT NULL,
  `is_finished` tinyint(1) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL,
  `change_reason` text,
  PRIMARY KEY (`sequence`),
  KEY `event_history_change_ts_index` (`change_ts`),
  KEY `event_history_project_id_change_ts_index` (`project_id`,`change_ts`)
) ENGINE=InnoDB AUTO_INCREMENT=19 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_history`
--

LOCK TABLES `event_history` WRITE;
/*!40000 ALTER TABLE `event_history` DISABLE KEYS */;
INSERT INTO `event_history` VALUES (1,0,'2018-08-25 12:48:23',1,1,1,'asf','gasdfgafsg','asgargrwegrwegwergwergvwer','2018-08-25 12:48:23','Briefing',0,0,NULL),(2,1,'2018-08-25 12:48:35',1,1,1,'asf','gasdfgafsg','asgargrwegrwegwergwergvwer','2018-08-25 12:48:23','Meeting',0,0,NULL),(3,0,'2018-08-25 17:04:44',1,2,1,'adsf','fasdfa','asdfasd','2018-09-02 00:00:00','Briefing',0,0,NULL),(4,0,'2018-08-25 21:51:41',1,3,1,'adsf','fasdfa','asdfasd','2018-09-02 00:00:00','Briefing',0,0,NULL),(5,0,'2018-08-25 22:23:59',1,4,4,'fasdfe','fds','asdf','2018-09-02 00:00:00','Briefing',0,0,NULL),(6,0,'2018-08-25 22:33:26',1,5,1,'this is a test','asdgf','aetvbvcb','2018-09-15 00:00:00','Conference',0,0,NULL),(7,1,'2018-08-25 23:36:36',1,1,1,'asf','gasdfgafsg','asgargrwegrwegwergwergvwer','2018-08-25 12:48:23','Meeting',1,0,NULL),(8,0,'2018-08-25 23:52:37',1,6,1,'testing','asdfgasd','asdgfasd','2018-09-05 00:00:00','Meeting',0,0,NULL),(9,1,'2018-08-25 23:58:39',1,3,1,'adsf','fasdfa','asdfasd','2018-09-02 00:00:00','Briefing',0,0,''),(10,1,'2018-08-25 23:58:48',1,3,1,'4681365','fasdfa','asdfasd','2018-09-02 00:00:00','Briefing',0,0,''),(11,1,'2018-08-26 00:03:30',1,6,1,'testing','asdfgasd','asdgfasd','2018-09-05 00:00:00','Meeting',0,0,''),(12,1,'2018-08-26 00:04:18',1,2,1,'adsf','fasdfa','asdfasd','2018-09-02 00:00:00','Briefing',0,0,''),(13,1,'2018-08-26 00:05:09',1,2,1,'adsf','fasdfa','asdfasd','2018-09-02 00:00:00','Briefing',1,0,''),(14,1,'2018-08-26 00:05:14',1,1,1,'asf','gasdfgafsg','asgargrwegrwegwergwergvwer','2018-08-25 12:48:23','Meeting',0,0,''),(15,1,'2018-08-26 00:06:03',1,5,1,'this is a test','asdgf','aetvbvcb','2018-09-15 00:00:00','Conference',1,0,''),(16,1,'2018-08-26 00:12:26',1,3,1,'4681365','fasdfa','asdfasd','2018-09-02 00:00:00','Briefing',0,1,''),(17,1,'2018-08-26 12:32:29',1,4,4,'fasdfe','fds','asdf','2018-09-02 00:00:00','Briefing',1,0,''),(18,0,'2018-08-27 21:00:17',1,7,1,'<b>test xss</b>','<b>test xss</b>','<b>test xss</b>','2018-08-27 21:00:17','Briefing',0,0,NULL);
/*!40000 ALTER TABLE `event_history` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `event_type`
--

DROP TABLE IF EXISTS `event_type`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `event_type` (
  `type_name` varchar(255) NOT NULL,
  PRIMARY KEY (`type_name`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `event_type`
--

LOCK TABLES `event_type` WRITE;
/*!40000 ALTER TABLE `event_type` DISABLE KEYS */;
INSERT INTO `event_type` VALUES ('Briefing'),('Conference'),('Meeting');
/*!40000 ALTER TABLE `event_type` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `privilege`
--

DROP TABLE IF EXISTS `privilege`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `privilege` (
  `privilege` varchar(32) NOT NULL,
  PRIMARY KEY (`privilege`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `privilege`
--

LOCK TABLES `privilege` WRITE;
/*!40000 ALTER TABLE `privilege` DISABLE KEYS */;
INSERT INTO `privilege` VALUES ('FULL_ACCESS'),('GRANT'),('READ'),('WRITE');
/*!40000 ALTER TABLE `privilege` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `privilege_dependency`
--

DROP TABLE IF EXISTS `privilege_dependency`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `privilege_dependency` (
  `privilege_parent` varchar(32) NOT NULL,
  `privilege_child` varchar(32) NOT NULL,
  PRIMARY KEY (`privilege_parent`,`privilege_child`),
  KEY `privilege_dependency_privilege_privilege_fk_2` (`privilege_child`),
  CONSTRAINT `privilege_dependency_privilege_privilege_fk` FOREIGN KEY (`privilege_parent`) REFERENCES `privilege` (`privilege`) ON UPDATE CASCADE,
  CONSTRAINT `privilege_dependency_privilege_privilege_fk_2` FOREIGN KEY (`privilege_child`) REFERENCES `privilege` (`privilege`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `privilege_dependency`
--

LOCK TABLES `privilege_dependency` WRITE;
/*!40000 ALTER TABLE `privilege_dependency` DISABLE KEYS */;
INSERT INTO `privilege_dependency` VALUES ('FULL_ACCESS','GRANT'),('FULL_ACCESS','READ'),('WRITE','READ'),('FULL_ACCESS','WRITE');
/*!40000 ALTER TABLE `privilege_dependency` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `project`
--

DROP TABLE IF EXISTS `project`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `project` (
  `project_id` int(11) NOT NULL AUTO_INCREMENT,
  `project_name` text,
  `last_changed_by` int(11) NOT NULL,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`project_id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `project`
--

LOCK TABLES `project` WRITE;
/*!40000 ALTER TABLE `project` DISABLE KEYS */;
INSERT INTO `project` VALUES (1,'test',1,0),(4,'gasas',5,0);
/*!40000 ALTER TABLE `project` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `login` varchar(255) DEFAULT NULL,
  `user_password` text,
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`id`),
  UNIQUE KEY `user_login_uindex` (`login`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (1,'zaec','zaec',0),(2,'qwerty','qwerty',0);
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user_project_privilege`
--

DROP TABLE IF EXISTS `user_project_privilege`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_project_privilege` (
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `privilege` varchar(32) DEFAULT NULL,
  `last_changed_by` int(11) NOT NULL,
  PRIMARY KEY (`user_id`,`project_id`),
  KEY `user_project_privilege_project_project_id_fk` (`project_id`),
  KEY `user_project_privilege_privilege_privilege_fk` (`privilege`),
  KEY `user_project_privilege_user_id_fk_2` (`last_changed_by`),
  CONSTRAINT `user_project_privilege_privilege_privilege_fk` FOREIGN KEY (`privilege`) REFERENCES `privilege` (`privilege`) ON UPDATE CASCADE,
  CONSTRAINT `user_project_privilege_project_project_id_fk` FOREIGN KEY (`project_id`) REFERENCES `project` (`project_id`) ON UPDATE CASCADE,
  CONSTRAINT `user_project_privilege_user_id_fk` FOREIGN KEY (`user_id`) REFERENCES `user` (`id`) ON UPDATE CASCADE,
  CONSTRAINT `user_project_privilege_user_id_fk_2` FOREIGN KEY (`last_changed_by`) REFERENCES `user` (`id`) ON UPDATE CASCADE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_project_privilege`
--

LOCK TABLES `user_project_privilege` WRITE;
/*!40000 ALTER TABLE `user_project_privilege` DISABLE KEYS */;
INSERT INTO `user_project_privilege` VALUES (1,1,'FULL_ACCESS',1),(1,4,'READ',1),(2,1,'READ',1),(2,4,'READ',1);
/*!40000 ALTER TABLE `user_project_privilege` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER insert_user_project_privilege AFTER INSERT ON user_project_privilege
FOR EACH ROW BEGIN
  INSERT INTO user_project_privilege_history (change_type, change_ts, change_by, user_id, project_id, privilege)
  VALUES (0, NOW(), NEW.last_changed_by, NEW.user_id, NEW.project_id, NEW.privilege);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER update_user_project_privilege AFTER UPDATE ON user_project_privilege
FOR EACH ROW BEGIN
  INSERT INTO user_project_privilege_history (change_type, change_ts, change_by, user_id, project_id, privilege)
  VALUES (1, NOW(), NEW.last_changed_by, NEW.user_id, NEW.project_id, NEW.privilege);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `user_project_privilege_history`
--

DROP TABLE IF EXISTS `user_project_privilege_history`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user_project_privilege_history` (
  `sequence` int(11) NOT NULL AUTO_INCREMENT,
  `change_type` tinyint(4) NOT NULL DEFAULT '0',
  `change_ts` datetime DEFAULT CURRENT_TIMESTAMP,
  `change_by` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `project_id` int(11) NOT NULL,
  `privilege` varchar(32) DEFAULT NULL,
  PRIMARY KEY (`sequence`),
  KEY `user_project_privilege_history_change_ts_index` (`change_ts`),
  KEY `user_project_privilege_history_project_id_change_ts_index` (`project_id`,`change_ts`)
) ENGINE=InnoDB AUTO_INCREMENT=20 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user_project_privilege_history`
--

LOCK TABLES `user_project_privilege_history` WRITE;
/*!40000 ALTER TABLE `user_project_privilege_history` DISABLE KEYS */;
INSERT INTO `user_project_privilege_history` VALUES (1,0,'2018-08-25 12:33:01',1,1,1,'FULL_ACCESS'),(2,1,'2018-08-25 12:33:16',1,1,1,'GRANT'),(3,1,'2018-08-25 12:33:26',1,1,1,'FULL_ACCESS'),(4,0,'2018-08-25 22:07:21',1,1,4,'READ'),(5,1,'2018-08-25 22:07:49',1,1,4,'FULL_ACCESS'),(6,1,'2018-08-25 22:08:03',1,1,4,'READ'),(7,1,'2018-08-26 10:35:35',1,1,4,'FULL_ACCESS'),(8,0,'2018-08-26 11:00:51',1,2,1,'READ'),(9,0,'2018-08-26 12:02:59',1,2,1,'READ'),(10,0,'2018-08-26 12:03:09',1,2,1,'WRITE'),(11,0,'2018-08-26 12:04:41',1,2,1,NULL),(12,0,'2018-08-26 12:04:56',1,2,1,'READ'),(13,0,'2018-08-26 12:11:33',1,2,1,'WRITE'),(14,0,'2018-08-26 12:12:43',1,2,1,NULL),(15,0,'2018-08-26 12:12:53',1,2,1,'READ'),(16,0,'2018-08-26 12:32:03',1,2,1,'WRITE'),(17,0,'2018-08-26 12:32:11',1,2,4,'READ'),(18,0,'2018-08-27 21:46:40',1,2,1,'READ'),(19,0,'2018-08-27 22:08:41',1,1,4,'READ');
/*!40000 ALTER TABLE `user_project_privilege_history` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-08-27 22:30:29
