let eventForm = {
    clear: modal => {
        ['input', 'textarea', 'select'].map(elem =>
            modal.find(elem).val("").prop('disabled', false)
        );
        modal.find('#inputEventIsFinished').prop('checked', false);

        modal.find('.btn').removeClass('disabled');
    },

    disable: modal => {
        ['input', 'textarea', 'select'].map(elem =>
            modal.find(elem).prop('disabled', true)
        );

        modal.find('.btn').addClass('disabled');
    },

    showCreateButtons: modal => {
        modal.find('#changeReasonRow').hide();
        modal.find('#updateEvent').hide();
        modal.find('#deleteEvent').hide();
        modal.find('#createEvent').show();
    },

    showUpdateButtons: modal => {
        modal.find('#changeReasonRow').show();
        modal.find('#updateEvent').show();
        modal.find('#deleteEvent').show();
        modal.find('#createEvent').hide();
    },

    fillForm: (modal, data) => {
        modal.find('#inputEventTitle').val(data.eventName);
        modal.find('#inputEventDate').val(data.startDate);
        modal.find('#inputEventType').val(data.type);
        modal.find('#inputEventProject').val(data.projectId);
        modal.find('#inputEventSummary').val(data.summary);
        modal.find('#inputEventDescription').val(data.description);
        modal.find('#inputEventIsFinished').prop('checked', data.isFinished);
        modal.find('#inputEventId').val(data.eventId);
    },
};

let grantForm = {
    editGrant: (project, login, currentGrant) => {
        $('#editGrantsWnd').modal('show');
        $('#editGrantsWnd').find('#inputGrantUser').val(login);
        $('#editGrantsWnd').find('#inputGrantAccess').val(currentGrant);
        $('#editGrantsWnd').find('#inputProjectId').val(project);
    },

    dismiss: (project, login) => {
        $.get("/editGrants", {user: login, project_id: project}, document.location.reload());
    }
};

$('#editEventWnd').on('show.bs.modal', function (event) {
    let button = $(event.relatedTarget);
    let event_id = button.data('event');
    let modal = $(this);

    eventForm.clear(modal);

    if (!event_id) {
        eventForm.showCreateButtons(modal);
    } else {
        eventForm.showUpdateButtons(modal);
        $.get("/event", {id: event_id}, response => {
            eventForm.fillForm(modal, response.response.event);
            if (response.response.privilege.indexOf('WRITE') === -1) {
                eventForm.disable(modal);
            }
        });
    }
});

$('#createEvent').click(() => {
    let data = $('#editEvent').serializeArray();

    $.get("/newEvent", data, () => document.location.reload());
});

$('#updateEvent').click(() => {
    let data = $('#editEvent').serializeArray();

    $.get("/updateEvent", data, () => document.location.reload());
});


$('#deleteEvent').click(() => {
    let data = $('#editEvent').serializeArray();

    $.get("/deleteEvent", data, () => document.location.reload());
});

$('.list-project').click(function () {
    let project = $(this).data('project-id');

    $.get("/getGrants", {project: project}, response => {
        let grants = response.response;
        let html = grants.map(grant => `
            <li class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                    <h5>${grant.login}</h5>
                    <p>
                        <a class="btn btn-primary btn-sm" href="javascript://" onclick="grantForm.editGrant(${project}, '${grant.login}', '${grant.privilege}')">Edit</a>
                        <a class="btn btn-danger btn-sm" href="javascript://" onclick="grantForm.dismiss(${project}, '${grant.login}')">Dismiss</a>
                    </p>
                </div>
                <span class="badge badge-primary">${grant.privilege}</span>
            </li>
        `).reduce((acc, cur) => acc + cur, "");
        $('#grants-project' + project).html(html);
    });
});

$('#grantSave').click(() => {
    let data = $('#editGrants').serializeArray();

    $('#grantsError').slideUp();
    $.get("/editGrants", data, response => {
        if (!response.error) {
            document.location.reload();
        } else {
            $('#grantsError').html(response.error).slideDown();
        }
    });
});

$('#logsWnd').on('show.bs.modal', function (event) {
    let button = $(event.relatedTarget);
    let project = button.data('project');

    $.get("/showLogs", {project: project}, response => {
        let grantLog = response.response.grant.map(
            row => `<td>${row.change_ts}</td><td>${row.admin}</td><td>${row.user}</td><td>${row.privilege}</td>`
        ).reduce((acc, cur) => acc + `<tr>${cur}</tr>`, '');
        $('#grantLog').html(`
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th scope="col">Date</th>
                        <th scope="col">Change by</th>
                        <th scope="col">User</th>
                        <th scope="col">New privilege</th>
                    </tr>
                </thead>
                <tbody>
                    ${grantLog}
                </tbody>
            </table>
        `);


        let eventLog = response.response.event
            .map(
                row => `
                    <td>${row.change_ts}</td>
                    <td>${row.admin}</td>
                    <td>${row.event_id}</td>
                    <td>${row.event_name.replace(/</g, '&lt;')}</td>
                    <td>${row.start_date}</td>
                    <td>${row.is_finished}</td>
                    <td>${row.is_deleted}</td>
                    <td>${row.change_reason ? row.change_reason.replace(/</g, '&lt;') : ""}</td>
                `
            )
            .reduce((acc, cur) => acc + `<tr>${cur}</tr>`, '');
        $('#eventLog').html(`
            <table class="table table-sm">
                <thead>
                    <tr>
                        <th scope="col">Date</th>
                        <th scope="col">Change by</th>
                        <th scope="col">Event id</th>
                        <th scope="col">Event name</th>
                        <th scope="col">Start date</th>
                        <th scope="col">Is finished</th>
                        <th scope="col">Is deleted</th>
                        <th scope="col">Reason</th>
                    </tr>
                </thead>
                <tbody>
                    ${eventLog}
                </tbody>
            </table>
        `);
    });
});