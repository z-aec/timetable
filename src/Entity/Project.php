<?php

declare(strict_types = 1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
  * Class Project
  *
  * @ORM\Entity
  * @ORM\Table(name="project")
  */
class Project
{
   /**
    * Project id
    *
    * @var int
    *
    * @ORM\Column(name="project_id", type="integer")
    * @ORM\Id
    */
   private $projectId;

   /**
    * Project name
    *
    * @var string
    *
    * @ORM\Column(name="project_name", type="string")
    */
   private $projectName;

   /**
    * Last changed by
    *
    * @var int
    *
    * @ORM\Column(name="last_changed_by", type="integer")
    */
   private $lastChangedBy;

   /**
    * Is deleted
    *
    * @var bool
    *
    * @ORM\Column(name="is_deleted", type="boolean")
    */
   private $isDeleted;


   /**
    * Get project id
    *
    * @return int
    */
   public function getProjectId(): int
   {
       return $this->projectId;
   }

   /**
    * Set project id
    *
    * @param int $projectId Project id
    *
    * @return void
    */
   public function setProjectId(int $projectId)
   {
       $this->projectId = $projectId;
   }

   /**
    * Get project name
    *
    * @return string
    */
   public function getProjectName(): string
   {
       return $this->projectName;
   }

   /**
    * Set project name
    *
    * @param string $projectName Project name
    *
    * @return void
    */
   public function setProjectName(string $projectName)
   {
       $this->projectName = $projectName;
   }

   /**
    * Get last changed by
    *
    * @return int
    */
   public function getLastChangedBy(): int
   {
       return $this->lastChangedBy;
   }

   /**
    * Set last changed by
    *
    * @param int $lastChangedBy Last changed by
    *
    * @return void
    */
   public function setLastChangedBy(int $lastChangedBy)
   {
       $this->lastChangedBy = $lastChangedBy;
   }

   /**
    * Get is deleted
    *
    * @return bool
    */
   public function getIsDeleted(): bool
   {
       return $this->isDeleted;
   }

   /**
    * Set is deleted
    *
    * @param bool $isDeleted Is deleted
    *
    * @return void
    */
   public function setIsDeleted(bool $isDeleted)
   {
       $this->isDeleted = $isDeleted;
   }

}
