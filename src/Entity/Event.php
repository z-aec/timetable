<?php

declare(strict_types = 1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
  * Class Event
  *
  * @ORM\Entity
  * @ORM\Table(name="event")
  */
class Event
{
   /**
    * Event id
    *
    * @var int
    *
    * @ORM\Column(name="event_id", type="integer")
    * @ORM\Id
    * @ORM\GeneratedValue(strategy="AUTO")
    */
   private $eventId;

   /**
    * Project id
    *
    * @var int
    *
    * @ORM\Column(name="project_id", type="integer")
    */
   private $projectId;

   /**
    * Event name
    *
    * @var string
    *
    * @ORM\Column(name="event_name", type="string")
    */
   private $eventName;

   /**
    * Summary
    *
    * @var string
    *
    * @ORM\Column(name="summary", type="string")
    */
   private $summary;

   /**
    * Description
    *
    * @var string
    *
    * @ORM\Column(name="description", type="string")
    */
   private $description;

   /**
    * Start date
    *
    * @var \DateTime
    *
    * @ORM\Column(name="start_date", type="datetime")
    */
   private $startDate;

   /**
    * Type
    *
    * @var string
    *
    * @ORM\Column(name="type", type="string")
    */
   private $type;

   /**
    * Is finished
    *
    * @var bool
    *
    * @ORM\Column(name="is_finished", type="boolean")
    */
   private $isFinished;

   /**
    * Is deleted
    *
    * @var bool
    *
    * @ORM\Column(name="is_deleted", type="boolean")
    */
   private $isDeleted;

   /**
    * Change reason
    *
    * @var string
    *
    * @ORM\Column(name="change_reason", type="string")
    */
   private $changeReason;

   /**
    * Last changed by
    *
    * @var int
    *
    * @ORM\Column(name="last_changed_by", type="integer")
    */
   private $lastChangedBy;


   /**
    * Get event id
    *
    * @return int
    */
   public function getEventId(): int
   {
       return $this->eventId;
   }

   /**
    * Set event id
    *
    * @param int $eventId Event id
    *
    * @return void
    */
   public function setEventId(int $eventId)
   {
       $this->eventId = $eventId;
   }

   /**
    * Get project id
    *
    * @return int
    */
   public function getProjectId(): int
   {
       return $this->projectId;
   }

   /**
    * Set project id
    *
    * @param int $projectId Project id
    *
    * @return void
    */
   public function setProjectId(int $projectId)
   {
       $this->projectId = $projectId;
   }

   /**
    * Get event name
    *
    * @return string
    */
   public function getEventName(): string
   {
       return $this->eventName;
   }

   /**
    * Set event name
    *
    * @param string $eventName Event name
    *
    * @return void
    */
   public function setEventName(string $eventName)
   {
       $this->eventName = $eventName;
   }

   /**
    * Get summary
    *
    * @return string
    */
   public function getSummary(): string
   {
       return $this->summary;
   }

   /**
    * Set summary
    *
    * @param string $summary Summary
    *
    * @return void
    */
   public function setSummary(string $summary)
   {
       $this->summary = $summary;
   }

   /**
    * Get description
    *
    * @return string
    */
   public function getDescription(): string
   {
       return $this->description;
   }

   /**
    * Set description
    *
    * @param string $description Description
    *
    * @return void
    */
   public function setDescription(string $description)
   {
       $this->description = $description;
   }

   /**
    * Get start date
    *
    * @return \DateTime
    */
   public function getStartDate(): \DateTime
   {
       return $this->startDate;
   }

   /**
    * Set start date
    *
    * @param \DateTime $startDate Start date
    *
    * @return void
    */
   public function setStartDate(\DateTime $startDate)
   {
       $this->startDate = $startDate;
   }

   /**
    * Get type
    *
    * @return string
    */
   public function getType(): string
   {
       return $this->type;
   }

   /**
    * Set type
    *
    * @param string $type Type
    *
    * @return void
    */
   public function setType(string $type)
   {
       $this->type = $type;
   }

   /**
    * Get is finished
    *
    * @return bool
    */
   public function getIsFinished(): bool
   {
       return $this->isFinished;
   }

   /**
    * Set is finished
    *
    * @param bool $isFinished Is finished
    *
    * @return void
    */
   public function setIsFinished(bool $isFinished)
   {
       $this->isFinished = $isFinished;
   }

   /**
    * Get is deleted
    *
    * @return bool
    */
   public function getIsDeleted(): bool
   {
       return $this->isDeleted;
   }

   /**
    * Set is deleted
    *
    * @param bool $isDeleted Is deleted
    *
    * @return void
    */
   public function setIsDeleted(bool $isDeleted)
   {
       $this->isDeleted = $isDeleted;
   }

   /**
    * Get change reason
    *
    * @return string|null
    */
   public function getChangeReason(): ?string
   {
       return $this->changeReason;
   }

   /**
    * Set change reason
    *
    * @param string $changeReason Change reason
    *
    * @return void
    */
   public function setChangeReason(string $changeReason)
   {
       $this->changeReason = $changeReason;
   }

   /**
    * Get last changed by
    *
    * @return int
    */
   public function getLastChangedBy(): int
   {
       return $this->lastChangedBy;
   }

   /**
    * Set last changed by
    *
    * @param int $lastChangedBy Last changed by
    *
    * @return void
    */
   public function setLastChangedBy(int $lastChangedBy)
   {
       $this->lastChangedBy = $lastChangedBy;
   }

}
