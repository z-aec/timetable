<?php

declare(strict_types=1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Security\Core\User\UserInterface;

/**
 * Class User
 *
 * @ORM\Entity
 * @ORM\Table(name="user")
 */
class User implements UserInterface, \Serializable
{
    /**
     * Id
     *
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     */
    private $id;

    /**
     * Login
     *
     * @var string
     *
     * @ORM\Column(name="login", type="string")
     */
    private $login;

    /**
     * User password
     *
     * @var string
     *
     * @ORM\Column(name="user_password", type="string")
     */
    private $userPassword;

    /**
     * Is deleted
     *
     * @var bool
     *
     * @ORM\Column(name="is_deleted", type="boolean")
     */
    private $isDeleted;


    public function __construct()
    {
        $this->isDeleted = false;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * Set id
     *
     * @param int $id Id
     *
     * @return void
     */
    public function setId(int $id)
    {
        $this->id = $id;
    }

    /**
     * Get login
     *
     * @return string
     */
    public function getUsername(): string
    {
        return $this->login;
    }

    /**
     * Set login
     *
     * @param string $login Login
     *
     * @return void
     */
    public function setUsername(string $login)
    {
        $this->login = $login;
    }

    /**
     * Get user password
     *
     * @return string
     */
    public function getPassword(): string
    {
        return $this->userPassword;
    }

    /**
     * Set user password
     *
     * @param string $userPassword User password
     *
     * @return void
     */
    public function setPassword(string $userPassword)
    {
        $this->userPassword = $userPassword;
    }

    /**
     * Get is deleted
     *
     * @return bool
     */
    public function getIsDeleted(): bool
    {
        return $this->isDeleted;
    }

    /**
     * Set is deleted
     *
     * @param bool $isDeleted Is deleted
     *
     * @return void
     */
    public function setIsDeleted(bool $isDeleted)
    {
        $this->isDeleted = $isDeleted;
    }

    public function getSalt()
    {
        return null;
    }

    public function getRoles()
    {
        return ['ROLE_USER'];
    }

    public function eraseCredentials()
    {
    }

    /** @see \Serializable::serialize() */
    public function serialize()
    {
        return serialize(array(
            $this->id,
            $this->login,
            $this->userPassword,
            // see section on salt below
            // $this->salt,
        ));
    }

    /** @see \Serializable::unserialize() */
    public function unserialize($serialized)
    {
        list (
            $this->id,
            $this->login,
            $this->userPassword,
            // see section on salt below
            // $this->salt
            ) = unserialize($serialized, array('allowed_classes' => false));
    }
}
