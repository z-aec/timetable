<?php

declare(strict_types = 1);

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
  * Class EventType
  *
  * @ORM\Entity
  * @ORM\Table(name="event_type")
  */
class EventType
{
   /**
    * Type name
    *
    * @var string
    *
    * @ORM\Column(name="type_name", type="string")
    * @ORM\Id
    */
   private $typeName;


   /**
    * Get type name
    *
    * @return string
    */
   public function getTypeName(): string
   {
       return $this->typeName;
   }

   /**
    * Set type name
    *
    * @param string $typeName Type name
    *
    * @return void
    */
   public function setTypeName(string $typeName)
   {
       $this->typeName = $typeName;
   }

}
