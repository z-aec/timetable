<?php
/**
 * Created by PhpStorm.
 * User: zaec
 * Date: 25.08.2018
 * Time: 16:49
 */

namespace App\Dto;


class EventDto
{
    public $eventId;
    public $eventName;
    public $startDate;
    public $type;
    public $project;
    public $summary;
    public $description;
    public $isFinished;
    public $changeReason;
    public $user;
}