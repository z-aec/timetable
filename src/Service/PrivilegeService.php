<?php
/**
 * Created by PhpStorm.
 * User: zaec
 * Date: 25.08.2018
 * Time: 16:35
 */

namespace App\Service;

use App\Enum\PrivilegeEnum;
use Doctrine\DBAL\Connection;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Class PrivilegeService
 * @package App\Service
 */
class PrivilegeService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * PrivilegeService constructor.
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param int $user
     * @param int $project
     * @param string $privilege
     * @return bool
     * @throws \Doctrine\DBAL\DBALException
     */
    public function hasAccess(int $user, int $project, string $privilege): bool
    {
        $query = "
            SELECT
                IF(count(*), 1, 0) AS has_access
            FROM
                user_project_privilege upp
                LEFT JOIN privilege_dependency pd ON upp.privilege = pd.privilege_parent
            WHERE
                upp.user_id = :user 
                AND upp.project_id = :project
                AND (upp.privilege = :privilege OR pd.privilege_child = :privilege)
        ";

        $stmt = $this->em->getConnection()->executeQuery(
            $query,
            [
                'user' => $user,
                'project' => $project,
                'privilege' => $privilege
            ]
        );

        return (bool) $stmt->fetch()['has_access'];
    }

    /**
     * @param int $user
     * @param array|null $projects
     *
     * @return int[][] Project ids indexed by privilege type
     * @see PrivilegeEnum
     */
    public function getAvailableProjects(int $user, ?array $projects = null): array
    {
        $qb = $this->em->getConnection()->createQueryBuilder();

        $qb->select([
            'upp.project_id',
            'upp.privilege',
            'pd.privilege_child'
        ])
            ->from('user_project_privilege', 'upp')
            ->leftJoin('upp', 'privilege_dependency', 'pd', 'upp.privilege = pd.privilege_parent')
            ->where('upp.user_id = :user')->setParameter('user', $user);

        if ($projects !== null) {
            $qb->andWhere('upp.project_id IN (:projects)')
                ->setParameter('projects', $projects, Connection::PARAM_INT_ARRAY);
        }

        $stmt = $qb->execute();

        $result = [];
        while (($row = $stmt->fetch())) {
            $result[$row['privilege']][$row['project_id']] = $row['project_id'];

            if (!empty($row['privilege_child'])) {
                $result[$row['privilege_child']][$row['project_id']] = $row['project_id'];
            }
        }

        return $result;
    }

    /**
     * @param int $project
     * @return array
     * [
     *      [
     *          'id'        => user id,
     *          'login'     => user login
     *          'privilege' => user main privilege
     *      ]
     * ]
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getProjectGrants(int $project): array
    {
        $query = "
            SELECT
                u.id,
                u.login,
                upp.privilege
            FROM 
                user_project_privilege upp
                JOIN `user` u ON upp.user_id = u.id
            WHERE
                upp.privilege IS NOT NULL 
                AND upp.project_id = :project
        ";

        $stmt = $this->em->getConnection()->executeQuery(
            $query,
            [
                'project' => $project,
            ]
        );

        return $stmt->fetchAll();
    }

    /**
     * @return string[]
     * @see PrivilegeEnum
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getAllGrants(): array
    {
        $query = "SELECT privilege FROM privilege";
        $stmt = $this->em->getConnection()->executeQuery($query);

        return $stmt->fetchAll(\PDO::FETCH_COLUMN);
    }

    /**
     * @param int $user user id to change privilege
     * @param int $project project id
     * @param null|string $privilege privilege (null means revoke all privileges)
     * @param int $by admin id
     * @return bool
     * @throws \Doctrine\DBAL\DBALException
     */
    public function grant(int $user, int $project, ?string $privilege, int $by): bool
    {
        if (!$this->isPrivilegeExists($privilege)) {
            return false;
        }

        $query = "
            REPLACE INTO user_project_privilege (user_id, project_id, privilege, last_changed_by)
            VALUES (:user_id, :project_id, :privilege, :last_changed_by)
        ";

        $this->em->getConnection()->executeUpdate(
            $query,
            [
                'user_id'         => $user,
                'project_id'      => $project,
                'privilege'       => $privilege,
                'last_changed_by' => $by
            ]
        );

        return true;
    }

    /**
     * @param int $project
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getPrivilegeLogs(int $project): array
    {
        $query = "
            SELECT
                upph.change_ts,
                a.login AS admin,
                u.login AS user,
                upph.privilege
            FROM
                user_project_privilege_history upph
                INNER JOIN `user` u ON upph.user_id = u.id
                INNER JOIN `user` a ON upph.change_by = a.id
            WHERE
                project_id = :project
            ORDER BY change_ts DESC 
            LIMIT 1000
        ";

        $stmt = $this->em->getConnection()->executeQuery($query, ['project' => $project]);

        return $stmt->fetchAll();
    }

    /**
     * @param null|string $privilege
     * @return bool
     * @throws \Doctrine\DBAL\DBALException
     */
    private function isPrivilegeExists(?string $privilege): bool
    {
        return empty($privilege) || in_array($privilege, $this->getAllGrants());
    }
}
