<?php
/**
 * Created by PhpStorm.
 * User: zaec
 * Date: 25.08.2018
 * Time: 15:23
 */

namespace App\Service;


use App\Dto\EventDto;
use App\Entity\Event;
use App\Entity\EventType;
use App\Entity\User;
use App\Enum\PrivilegeEnum;
use Doctrine\ORM\EntityManagerInterface;
use App\Entity\Project;
use Psr\Log\InvalidArgumentException;
use Symfony\Component\HttpFoundation\File\Exception\AccessDeniedException;

/**
 * Class MainService
 * @package App\Service
 */
class MainService
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @var PrivilegeService
     */
    private $privilegeService;

    /**
     * MainService constructor.
     * @param EntityManagerInterface $em
     * @param PrivilegeService $privilegeService
     */
    public function __construct(EntityManagerInterface $em, PrivilegeService $privilegeService)
    {
        $this->em = $em;
        $this->privilegeService = $privilegeService;
    }

    /**
     * @param int $user
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getIndexParameters(int $user): array
    {
        $availableProjects = $this->privilegeService->getAvailableProjects($user);

        return [
            'eventTypes' => $this->getEventTypeList(),
            'projects'   => $this->getProjectList($availableProjects),
            'events'     => $this->getTimetable($availableProjects),
            'grants'     => $this->privilegeService->getAllGrants()
        ];
    }

    /**
     * @param int $user
     * @param int $id
     * @return array
     * [
     *      'event' => Event
     *      'privilege' => string[] List of event privileges
     * ]
     */
    public function getEvent(int $user, int $id): array
    {
        $event = $this->em->getRepository(Event::class)->findOneBy(['eventId' => $id]);

        if (!$event || !($event instanceof Event)) {
            throw new InvalidArgumentException("Event {$id} not found");
        }

        $privilege = $this->privilegeService->getAvailableProjects($user, [(int)$event->getProjectId()]);

        if (empty($privilege[PrivilegeEnum::READ])) {
            throw new AccessDeniedException("Access denied");
        }

        return ['event' => $event, 'privilege' => array_keys($privilege)];
    }

    /**
     * @param EventDto $parameters
     * @return bool
     * @throws \Doctrine\DBAL\DBALException
     */
    public function createEvent(EventDto $parameters): bool
    {
        if (!$this->privilegeService->hasAccess($parameters->user, $parameters->project, PrivilegeEnum::WRITE)) {
            throw new AccessDeniedException("Access denied");
        }

        $entity = new Event();
        $entity->setEventName($parameters->eventName);
        $entity->setProjectId($parameters->project);
        $entity->setType($parameters->type);
        $entity->setSummary($parameters->summary);
        $entity->setDescription($parameters->description);
        $entity->setStartDate($parameters->startDate);
        $entity->setIsFinished($parameters->isFinished);
        $entity->setIsDeleted(false);
        $entity->setLastChangedBy($parameters->user);

        $this->em->persist($entity);
        $this->em->flush();

        return true;
    }

    /**
     * @param EventDto $parameters
     * @return bool
     * @throws \Doctrine\DBAL\DBALException
     */
    public function updateEvent(EventDto $parameters): bool
    {
        $entity = $this->em->getRepository(Event::class)->findOneBy(['eventId' => $parameters->eventId]);
        if (!$entity || !($entity instanceof Event)) {
            throw new InvalidArgumentException("Event {$parameters->eventId} not found");
        }

        if (!$this->privilegeService->hasAccess($parameters->user, $entity->getProjectId(), PrivilegeEnum::WRITE)
            || !$this->privilegeService->hasAccess($parameters->user, $parameters->project, PrivilegeEnum::WRITE)
        ) {
            throw new AccessDeniedException("Access denied");
        }

        $entity->setEventName($parameters->eventName);
        $entity->setProjectId($parameters->project);
        $entity->setType($parameters->type);
        $entity->setSummary($parameters->summary);
        $entity->setDescription($parameters->description);
        $entity->setStartDate($parameters->startDate);
        $entity->setIsFinished($parameters->isFinished);
        $entity->setChangeReason($parameters->changeReason);
        $entity->setLastChangedBy($parameters->user);

        $this->em->persist($entity);
        $this->em->flush();

        return true;
    }

    /**
     * @param int $user
     * @param int $eventId
     * @return bool
     * @throws \Doctrine\DBAL\DBALException
     */
    public function deleteEvent(int $user, int $eventId): bool
    {
        $entity = $this->em->getRepository(Event::class)->findOneBy(['eventId' => $eventId]);
        if (!$entity || !($entity instanceof Event)) {
            throw new InvalidArgumentException("Event {$eventId} not found");
        }

        if (!$this->privilegeService->hasAccess($user, $entity->getProjectId(), PrivilegeEnum::WRITE)) {
            throw new AccessDeniedException("Access denied");
        }

        $entity->setIsDeleted(true);
        $this->em->persist($entity);
        $this->em->flush();

        return true;
    }

    /**
     * @param int $user
     * @param int $project
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getGrants(int $user, int $project): array
    {
        if (!$this->privilegeService->hasAccess($user, $project, PrivilegeEnum::GRANT)) {
            throw new AccessDeniedException("Access denied");
        }

        return $this->privilegeService->getProjectGrants($project);
    }

    /**
     * @param int $admin admin id
     * @param int $project project id
     * @param string $login user login
     * @param null|string $grant privilege name (null means revoke privilege)
     * @return bool
     * @throws \Doctrine\DBAL\DBALException
     */
    public function editGrants(int $admin, int $project, string $login, ?string $grant): bool
    {
        if (!$this->privilegeService->hasAccess($admin, $project, PrivilegeEnum::GRANT)) {
            throw new AccessDeniedException("Access denied");
        }

        $user = $this->em->getRepository(User::class)->findOneBy(['login' => $login]);
        if (!$user || ! ($user instanceof User)) {
            throw new \InvalidArgumentException("User not found");
        }

        return $this->privilegeService->grant($user->getId(), $project, $grant, $admin);
    }

    /**
     * @param int $admin
     * @param int $project
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getLogs(int $admin, int $project)
    {
        if (!$this->privilegeService->hasAccess($admin, $project, PrivilegeEnum::GRANT)) {
            throw new AccessDeniedException("Access denied");
        }

        return [
            'grant' => $this->privilegeService->getPrivilegeLogs($project),
            'event' => $this->getEventLogs($project),
        ];
    }

    /**
     * @param int $project
     * @return array
     * @throws \Doctrine\DBAL\DBALException
     */
    private function getEventLogs(int $project): array
    {
        $query = "
            SELECT
                eh.change_ts,
                a.login AS admin,
                eh.event_id,
                eh.event_name,
                eh.summary,
                eh.description,
                eh.start_date,
                eh.is_finished,
                eh.is_deleted,
                eh.change_reason
            FROM
                event_history eh
                INNER JOIN `user` a ON eh.change_by = a.id
            WHERE
                project_id = :project
            ORDER BY change_ts DESC 
            LIMIT 1000
        ";

        $stmt = $this->em->getConnection()->executeQuery($query, ['project' => $project]);

        return $stmt->fetchAll();
    }

    /**
     * @return string[]
     */
    private function getEventTypeList(): array
    {
        $eventTypes = $this->em->getRepository(EventType::class)->findAll();

        $result = [];
        foreach ($eventTypes as $eventType) {
            /** @var EventType $eventType */
            $result[] = $eventType->getTypeName();
        }

        return $result;
    }

    /**
     * @param array $projects projects id from PrivilegeService::getAvailableProjects
     * @return array
     * [
     *      [
     *          'id'         => project id
     *          'name'       => project name
     *          'writable'   => is writable (PrivilegeEnum::WRITE)
     *          'is_managed' => is managed (PrivilegeEnum::GRANT)
     *      ]
     * ]
     */
    private function getProjectList(array $projects): array
    {
        $projectEntities = $this->em->getRepository(Project::class)->findBy([
            'projectId' => $projects[PrivilegeEnum::READ],
            'isDeleted' => false,
        ]);

        $result = [];
        foreach ($projectEntities as $project) {
            /** @var Project $project */
            $result[] = [
                'id' => $project->getProjectId(),
                'name' => $project->getProjectName(),
                'writable' => !empty($projects[PrivilegeEnum::WRITE][$project->getProjectId()]),
                'is_managed' => !empty($projects[PrivilegeEnum::GRANT][$project->getProjectId()]),
            ];
        }

        return $result;
    }

    /**
     * @param array $projects projects id from PrivilegeService::getAvailableProjects
     * @return Event[][] indexed by week and project
     */
    private function getTimetable(array $projects): array
    {
        $events = $this->em->getRepository(Event::class)->findBy(
            [
                'projectId' => $projects[PrivilegeEnum::READ],
                'isDeleted' => false,
            ], [
                'startDate' => 'ASC'
            ]
        );

        $result = [];
        foreach ($events as $event) {
            /** @var Event $event */
            $weekDay = $event->getStartDate()->format('N') - 1;
            $weekMonday = (clone $event->getStartDate())->modify("-{$weekDay} days");
            $weekSunday = (clone $weekMonday)->modify("+6 days");
            $weekKey = $weekMonday->format('Y-m-d') . ' - ' . $weekSunday->format('Y-m-d');

            if (!isset($result[$weekKey][$event->getProjectId()])) {
                $result[$weekKey][$event->getProjectId()] = [];
            }

            $result[$weekKey][$event->getProjectId()][] = $event;
        }

        return $result;
    }
}
