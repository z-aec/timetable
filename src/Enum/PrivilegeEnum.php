<?php
/**
 * Created by PhpStorm.
 * User: zaec
 * Date: 25.08.2018
 * Time: 16:36
 */

namespace App\Enum;


class PrivilegeEnum
{
    const FULL_ACCESS = 'FULL_ACCESS';
    const GRANT = 'GRANT';
    const WRITE = 'WRITE';
    const READ = 'READ';
}