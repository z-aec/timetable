<?php
declare(strict_types=1);

namespace App\Controller;

use App\Dto\EventDto;
use App\Service\MainService;
use App\Service\PrivilegeService;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class MainController
 * @package App\Controller
 */
class MainController extends AbstractController
{
    /**
     * @var MainService
     */
    private $mainService;

    /**
     * @var PrivilegeService
     */
    private $privilegeService;

    /**
     * MainController constructor.
     * @param MainService $mainService
     * @param PrivilegeService $privilegeService
     */
    public function __construct(MainService $mainService, PrivilegeService $privilegeService)
    {
        $this->mainService = $mainService;
        $this->privilegeService = $privilegeService;
    }

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Doctrine\DBAL\DBALException
     */
    public function index()
    {
        return $this->render(
            'main/index.html.twig',
            $this->mainService->getIndexParameters((int) $this->getUser()->getId())
        );
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     */
    public function event(Request $request)
    {
        return $this->json([
            'response' => $this->mainService->getEvent(
                (int) $this->getUser()->getId(),
                (int) $request->query->get('id')
            )
        ]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Doctrine\DBAL\DBALException
     */
    public function newEvent(Request $request)
    {
        $result = $this->mainService->createEvent($this->createEventDto($request));

        return $this->json(['response' => $result]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Doctrine\DBAL\DBALException
     */
    public function updateEvent(Request $request)
    {
        $result = $this->mainService->updateEvent($this->createEventDto($request));

        return $this->json(['response' => $result]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Doctrine\DBAL\DBALException
     */
    public function deleteEvent(Request $request)
    {
        $result = $this->mainService->deleteEvent(
            (int) $this->getUser()->getId(),
            (int) $request->query->get('event_id')
        );

        return $this->json(['response' => $result]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Doctrine\DBAL\DBALException
     */
    public function getGrants(Request $request)
    {
        $result = $this->mainService->getGrants((int) $this->getUser()->getId(), (int) $request->query->get('project'));

        return $this->json(['response' => $result]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Doctrine\DBAL\DBALException
     */
    public function editGrants(Request $request)
    {
        $result = null;
        try {
            $result = $this->mainService->editGrants(
                (int) $this->getUser()->getId(),
                (int) $request->query->get('project_id'),
                $request->query->get('user'),
                $request->query->has('grant') ? $request->query->get('grant') : null
            );
        } catch (\InvalidArgumentException $e) {
            return $this->json(['error' => $e->getMessage()]);
        }

        return $this->json(['response' => $result]);
    }

    /**
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\JsonResponse
     * @throws \Doctrine\DBAL\DBALException
     */
    public function showLogs(Request $request)
    {
        $result = $this->mainService->getLogs(
            (int) $this->getUser()->getId(),
            (int) $request->query->get('project')
        );

        return $this->json(['response' => $result]);
    }

    /**
     * @param Request $request
     * @return EventDto
     */
    private function createEventDto(Request $request)
    {
        $params = new EventDto();

        $params->eventId     = $request->query->has('event_id') ? $request->query->get('event_id') : null;
        $params->eventName   = $request->query->get('title');
        $params->startDate   = new \DateTime($request->query->get('start_date'));
        $params->type        = $request->query->get('type');
        $params->project     = (int) $request->query->get('project');
        $params->summary     = $request->query->get('summary');
        $params->description = $request->query->get('description');
        $params->isFinished  = $request->query->has('is_finished');
        $params->user        = (int) $this->getUser()->getId();
        $params->changeReason = $request->query->has('change_reason') ? $request->query->get('change_reason') : "";

        return $params;
    }
}
